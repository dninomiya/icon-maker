# アイコンメイカー動作確認ツール

[http://pre.noob.jp](http://pre.noob.jp)

guest:guest

## 概要

ブラウザ上でアイコンのスタイル調整（色、サイズなど）を行い、PNG等の形式で書き出せるかを確認するためのツール

## 結果

可能

- SVGをフロントで調整し、サーバーに送り、各画像を生成することが可能
- フロントで画像変換する場合PNG,JPGのみ対応（IE9以降対応）
- サーバーで画像変換するImageMagickで可能な範囲で変換可能
- スタイル固定の静的なファイルは静的に出力し、アップ（`esp, psd, pdf`)
- イラレファイル(.ai)があればsvgを始めとする各ファイル形式に出力可能
- ベクターアイコンは一般的に(.ai)で作成されるが、イラレがないと編集できない。イラレがない場合SVGをはじめとする形式に出力したものも合わせて納品してもらう必要あり
- イラレのバッチで複数の.aiを機械的にSVGに〜などは可能

## 今回の実装

1. ネットで拾ったSVGアイコンをサーバーにアップ
2. JSでSVGのスタイル調整を行う
3. JS（Canvas）でSVGをjpeg,pngのdataurlに変換し、PHPに送信
4. PHPでdataurlを画像に変換し、ZIP圧縮
5. JSでzipを叩いてダウンロード

### 使用ライブラリ

|ライブラリ名|役割|
|---|---|
|jscolor.min.js|カラーピッカーで使用|
|StackBlur.js|svgをcanvasに取り込むため使用|
|rgbcolor.js|svgをcanvasに取り込むため使用|
|canvg.js|svgをcanvasに取り込むため使用|

全て無料 + 商用利用可能です

### 使用したSVG

[http://www.flaticon.com/packs/fitness-line-craft](http://www.flaticon.com/packs/fitness-line-craft)

- 実際に必要なファイル（形式、サイズ）はまさにこう言うファイルになります。
- 上記サイトのように`esp`, `psd`形式でダウンロードさせたい場合静的にアップロードする形になります

|形式|出力元|必要性|
|---|---|---|
|.svg|イラレ|必ず必要。|
|.ai|イラレ|ダウンロードさせたい場合静的アップ|
|.esp|イラレ|ダウンロードさせたい場合静的アップ|
|.psd|イラレ|ダウンロードさせたい場合静的アップ|
|base64|プログラム|svgからプログラムで動的生成|
|jpg|プログラム|svgからプログラムで動的生成|
|png|プログラム|svgからプログラムで動的生成|

## 補足

- http://www.flaticon.com/free-icon/
- http://customizr.net/icons/

ソースを見る限りこれらのサイトも同様に「SVGを修正 -> サーバーサイドで画像化」しているようです。

- 今は普及率低いですが、将来的に[WebP](https://ja.wikipedia.org/wiki/WebP)の需要はあるかもしれません
- アイコンフォントの場合デザインはcssのマークアップで制御するため、配布ソースは静的なフォントファイルでOKです。[SVGをアイコンフォントにする方法](http://unguis.cre8or.jp/web/6321)
- フロントのみでzip圧縮、ダウンロードをしようとしたが無理でした(safariなど一部ブラウザがzip圧縮、ダウンロードに対応していなかったため)
- あくまで動作確認の突貫ソースなのでソースは参考程度でお願いいたします。
- フロントでGif画像生成を行う場合File APIを使うため、対応ブラウザはGoogle ChromeやFirefox 17以上、Safari6以上、IE10、iOS6のMobile Safari以上に限定されます。

## 参考イメージ

![1](./docs/slide1.jpeg)
![2](./docs/slide2.jpeg)