// 固定値
var DEFAULT_COLOR = '#46C5F0';
var DEFAULT_MIME_TYPE = 'png'; // or jpeg

// 変数宣言
var $canvas = $('#canvas');
var mimeType = DEFAULT_MIME_TYPE;
var ctx = $canvas[0].getContext('2d');

// SVGファイル一覧(実際にはバックから受け取るであろうデータ)
var svgs = [
  'barbell-and-plates.svg',
  'bottle-of-water.svg',
  'boxing.svg',
  'cross-trainer.svg',
  'dumbbells.svg',
  'exercise-bike.svg',
  'fitness-app.svg',
  'fitness-ball.svg',
  'fitness-drinks.svg',
  'fitness-gloves.svg',
  'fitness-nutrition.svg',
  'fitness-shoes.svg',
  'fitness-step.svg',
  'fitness-tracker.svg',
  'floor-wheel.svg',
  'grippers.svg',
  'gym-station.svg',
  'gymnasium.svg',
  'jumping-rope.svg',
  'kettle-bell.svg',
  'power-tower.svg',
  'push-up-grips.svg',
  'rowing-machine.svg',
  'strandpulling.svg',
  'swedish-wall.svg',
  'training-bag.svg',
  'training-mat.svg',
  'treadmill.svg',
  'weight-bench.svg',
  'women-fitness-clothing.svg'
];

// SVG -> IMG
function getImage(tgt, format) {
  var canvas = document.getElementById('canvas');

  if (format === 'jpeg') {
    // canvas背景セット
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, 1500, 1500);
  }

  canvg(canvas, tgt.html(), {
    ignoreMouse: true,
    ignoreAnimation: true,
    ignoreClear: format === 'jpeg',
    renderCallback: function() {
      var dataurl = canvas.toDataURL('image/' + format);
      $.post('get.php', {
        dataurl: dataurl,
        filename: tgt.attr('data-filename') + '.' + format
      }, function(res) {
        location.href = res;
      });
    }
  });
}

// SVG実装
$.each(svgs, function() {

  var filename = this,
      path = 'svg/' + filename;

  // アイコン取得:描画
  $.get(path, function(svg) {
    $('.js-wrap').append('<div class="cell"><div class="icon" data-filename="' + filename.replace('.svg', '') + '">' + svg + '</div></div>');
    $('svg path').css('fill', DEFAULT_COLOR);
  }, 'text');

});

// カラー変更
$('.jscolor').attr('value', DEFAULT_COLOR).on('change', function(event) {
  $('svg path').css('fill', '#' + this.jscolor);
});

// アイコン取得
$('body').on('click', '.icon', function() {
  getImage($(this), mimeType);

  // 例外処理
  if (!$(this).find('svg')[0]) {
    return false;
  }
});

// 画像タイプ変更
$('.js-mime').on('change', function() {
  mimeType = $(this).val();
});

// サイズ変更
$('.js-size').on('change', function() {
  var size = $(this).val();
  $canvas.attr('width', size);
  $canvas.attr('height', size);
});
