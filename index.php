<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
  <link rel="stylesheet" href="css/style.css">
</head>
<body>

<!-- Controller -->
<header class="ctrl">
  <h1>Icon Getter(Beta)</h1>
  <p>Color: <input class="jscolor" value="fffff"></p>
  <select class="js-mime">
    <option value="png">png</option>
    <option value="jpeg">jpeg</option>
  </select>
  <select class="js-size">
    <option value="300">300x300</option>
    <option value="50">50x50</option>
    <option value="1500">1500x1500</option>
  </select>
</header>

<main>

  <canvas id="canvas" width="300px" height="300px" style="display: none;"></canvas>

  <!-- Icons -->
  <div class="grid js-wrap"></div>

</main>

<script src="js/jscolor.min.js"></script>
<script src="js/rgbcolor.js"></script>
<script src="js/StackBlur.js"></script>
<script src="js/canvg.js"></script>
<script src="js/app.js"></script>
</body>
</html>
