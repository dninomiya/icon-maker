<?php

if (isset($_POST["dataurl"])) {

	$data = $_POST["dataurl"];
	$filename = $_POST["filename"];
	$filePath = 'img/' . $filename;
	$zipFileName = 'img.zip';
	$zipTmpDir = './img/';
	$files = array($filePath);

	// データURLを元に画像を作成
	$encodedData = str_replace(' ','+',$data);
	$decodedData = base64_decode($encodedData);
	$uri = substr($encodedData, strpos($encodedData, ',') + 1);
	file_put_contents($filePath, base64_decode($uri));

	// 既存のzipを削除
	if (file_exists ($zipTmpDir.$zipFileName)) {
		unlink($zipTmpDir.$zipFileName);
	}

	// ZIP化
	$zip = new ZipArchive();
	$res = $zip->open($zipTmpDir.$zipFileName, ZipArchive::CREATE);

	if($res === true){
	    foreach($files as $file){
	        $zip->addFile($file);
	    }
	    $zip->close();

		// JSにzipのパスを返す
		echo $zipTmpDir.$zipFileName;

	} else {
	    echo 'Error Code: ' . $res;
	}

}
