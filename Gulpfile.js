var gulp = require('gulp'),
    connect = require('gulp-connect-php');

gulp.task('connect', function() {
  connect.server({
  	open: true
  });
});

gulp.task('default', ['connect']);
